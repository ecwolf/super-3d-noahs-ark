# Variables for generating version.h

set(PRODUCT_NAME "Noah3D")
set(PRODUCT_IDENTIFIER "org.ecwolf.Noah3D")
if(APPLE OR WIN32)
	set(PRODUCT_DIRECTORY "${PRODUCT_NAME}")
else()
	string(TOLOWER "${PRODUCT_NAME}" PRODUCT_DIRECTORY)
endif()

string(TOLOWER "${PRODUCT_NAME}" ENGINE_BINARY_NAME)

set(VERSION_MAJOR 1)
set(VERSION_MINOR 4)
set(VERSION_PATCH 1)
set(VERSION_STRING "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
